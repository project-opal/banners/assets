function mondayDisplayVideoBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<video autoplay loop muted class="mondaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</video>' + '</a>';
  }
  document.getElementById("displayMondayDBV").innerHTML = out;
}
function mondayDisplayBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<img class="mondaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</a>';
  }
  document.getElementById("displayMondayDB").innerHTML = out;
}

function tuesdayDisplayVideoBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<video autoplay loop muted class="tuesdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</video>' + '</a>';
  }
  document.getElementById("displayTuesdayDBV").innerHTML = out;
}
function tuesdayDisplayBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<img class="tuesdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</a>';
  }
  document.getElementById("displayTuesdayDB").innerHTML = out;
}

function wednesdayDisplayVideoBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<video autoplay loop muted class="wednesdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</video>' + '</a>';
  }
  document.getElementById("displayWednesdayDBV").innerHTML = out;
}
function wednesdayDisplayBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<img class="wednesdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</a>';
  }
  document.getElementById("displayWednesdayDB").innerHTML = out;
}

function thursdayDisplayVideoBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<video autoplay loop muted class="thursdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</video>' + '</a>';
  }
  document.getElementById("displayThursdayDBV").innerHTML = out;
}
function thursdayDisplayBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<img class="thursdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</a>';
  }
  document.getElementById("displayThursdayDB").innerHTML = out;
}

function fridayDisplayVideoBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<video autoplay loop muted class="fridaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</video>' + '</a>';
  }
  document.getElementById("displayFromFridayDBV").innerHTML = out;
}
function fridayDisplayBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<img class="fridaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</a>';
  }
  document.getElementById("displayFromFridayDB").innerHTML = out;
}

function saturdayDisplayVideoBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<video autoplay loop muted class="saturdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</video>' + '</a>';
  }
  document.getElementById("displayFromSaturdayDBV").innerHTML = out;
}
function saturdayDisplayBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<img class="saturdaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</a>';
  }
  document.getElementById("displayFromSaturdayDB").innerHTML = out;
}

function sundayDisplayVideoBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<video autoplay loop muted class="sundaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</video>' + '</a>';
  }
  document.getElementById("displayFromSundayDBV").innerHTML = out;
}
function sundayDisplayBanners(arr) {
  var out = "";
  var i;
  for(i = 0; i<arr.length; i++) {
    out += '<a href="' + arr[i].seriesURL + '">' + 
	'<img class="sundaySlides w3-animate-opacity" src="' + 
    arr[i].seriesBanner + '" style="width:100%">' + '</a>';
  }
  document.getElementById("displayFromSundayDB").innerHTML = out;
}