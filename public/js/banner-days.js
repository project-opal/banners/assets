var currentDate = new Date();
var currentMonth = currentDate.getMonth();
var currentDay = currentDate.getDay();
var currentHour = currentDate.getHours();
var currentMinute = currentDate.getMinutes();
var consoleMessage = 0;
document.addEventListener('DOMContentLoaded', function() {
  var monday = document.getElementById('monday');
  var tuesday = document.getElementById('tuesday');
  var wednesday = document.getElementById('wednesday');
  var thursday = document.getElementById('thursday');
  var friday = document.getElementById('friday');
  var saturday = document.getElementById('saturday');
  var sunday = document.getElementById('sunday');

  function openClose() {
    //reset display each time this function runs
    [monday, tuesday, wednesday, thursday, friday, saturday, sunday].forEach(function(element) {
      element.classList.remove('hidden');
    });
    //Monday Code Banners
    if (currentDay === 1) {
      //
      tuesday.classList.add('hidden');
      wednesday.classList.add('hidden');
      thursday.classList.add('hidden');
      friday.classList.add('hidden');
      saturday.classList.add('hidden');
      sunday.classList.add('hidden');
    }
    //Tuesday Code Banners
    else if (currentDay === 2) {
      monday.classList.add('hidden');
      //
      wednesday.classList.add('hidden');
      thursday.classList.add('hidden');
      friday.classList.add('hidden');
      saturday.classList.add('hidden');
      sunday.classList.add('hidden');      
    }
    //Wednesday Banner Code
    else if (currentDay === 3) {
      monday.classList.add('hidden');
      tuesday.classList.add('hidden');
      //
      thursday.classList.add('hidden');
      friday.classList.add('hidden');
      saturday.classList.add('hidden');
      sunday.classList.add('hidden');      
    }
    //Thursday Banner Code
    else if (currentDay === 4) {
      monday.classList.add('hidden');
      tuesday.classList.add('hidden');
      wednesday.classList.add('hidden');
      //
      friday.classList.add('hidden');
      saturday.classList.add('hidden');
      sunday.classList.add('hidden');      
    }
    //Firday Banner Code
    else if (currentDay === 5) {
      monday.classList.add('hidden');
      tuesday.classList.add('hidden');
      wednesday.classList.add('hidden');
      thursday.classList.add('hidden');
      //
      saturday.classList.add('hidden');
      sunday.classList.add('hidden');        
    }
    //Saturday Banner Code
    else if (currentDay === 6) {
      monday.classList.add('hidden');
      tuesday.classList.add('hidden');
      wednesday.classList.add('hidden');
      thursday.classList.add('hidden');
      friday.classList.add('hidden');
      //
      sunday.classList.add('hidden');        
    } 
    //Sunday Banner Code
    else if (currentDay === 0) {
      monday.classList.add('hidden');
      tuesday.classList.add('hidden');
      wednesday.classList.add('hidden');
      thursday.classList.add('hidden');
      friday.classList.add('hidden');
      saturday.classList.add('hidden');
      //
    }     
    else {
      monday.classList.add('hidden');
      tuesday.classList.add('hidden');
      wednesday.classList.add('hidden');
      thursday.classList.add('hidden');
      friday.classList.add('hidden');
      saturday.classList.add('hidden');
      sunday.classList.add('hidden');
    }
  }
  setInterval(openClose, 5000);
  openClose();
  errorCheck();
  consoleCheck();
});

function consoleCheck() {
    if (consoleMessage <= 0) {
     consoleMessage++;
    }
}    

function errorCheck() {
	if (consoleMessage <= 1 && currentDay === 1) {
      console.log('%cMonday Banners Loaded Successfully', 'background: green; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cAlso, DavidCarbon was here!', 'display: block; font-weight: bold; text-align: center; font-size:30px;');
    }
	else if (consoleMessage <= 1 && currentDay === 2) {
      console.log('%cTuesday Banners Loaded Successfully', 'background: green; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cAlso, DavidCarbon was here!', 'display: block; font-weight: bold; text-align: center; font-size:30px;');
    }
    else if (consoleMessage <= 1 && currentDay === 3) {
      console.log('%cWednesday Banners Loaded Successfully', 'background: green; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cAlso, DavidCarbon was here!', 'display: block; font-weight: bold; text-align: center; font-size:30px;');
    }  
    else if (consoleMessage <= 1 && currentDay === 4) {
      console.log('%cThursday Banners Loaded Successfully', 'background: green; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cAlso, DavidCarbon was here!', 'display: block; font-weight: bold; text-align: center; font-size:30px;');
    }  
    else if (consoleMessage <= 1 && currentDay === 5) {
      console.log('%cFriday Banners Loaded Successfully', 'background: green; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cAlso, DavidCarbon was here!', 'display: block; font-weight: bold; text-align: center; font-size:30px;'); 
    }  
    else if (consoleMessage <= 1 && currentDay === 6) {
      console.log('%cSaturday Banners Loaded Successfully', 'background: green; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cAlso, DavidCarbon was here!', 'display: block; font-weight: bold; text-align: center; font-size:30px;');
    }  
    else if (consoleMessage <= 1 && currentDay === 0) {
      console.log('%cSunday Banners Loaded Successfully', 'background: green; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cAlso, DavidCarbon was here!', 'display: block; font-weight: bold; text-align: center; font-size:30px;');
    }
    else {
      console.log('%cBanners Failed to Load Successfully', 'background: red; color: white; display: block; font-weight: bold; text-align: center; font-size:30px;');
      console.log('%cSeems like DavidCarbon Messed Up Somewhere In My Code!', 'display: block; font-weight: bold; text-align: center; font-size:30px;');
    }    
}